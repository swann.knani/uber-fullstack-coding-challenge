import { renderHook, act } from "@testing-library/react-hooks";
import useGiphyApi from "../effects/use-giphy-api";
import { GIFApi } from "../gif-api";
import { GIFObject } from "../fixtures";

const GiphyApi: jest.Mocked<GIFApi> = {
  search: jest.fn()
};

beforeEach(() => {
  GiphyApi.search.mockReset();
});

test("it should initialize state properly", () => {
  const { result } = renderHook(() => useGiphyApi(GiphyApi, "", 0));

  expect(result.current.state).toMatchObject({
    isLoading: false,
    isError: false,
    data: []
  });
});

test("it should not fetch the API when query is empty", () => {
  renderHook(() => useGiphyApi(GiphyApi, "", 0));

  expect(GiphyApi.search).not.toHaveBeenCalled();
});

test("it should trigger a search when query is set", async () => {
  const query = "Dog";
  const response = [GIFObject];
  GiphyApi.search.mockResolvedValue(response);

  const { result } = renderHook(() => useGiphyApi(GiphyApi, "", 0));

  await act(async () => {
    result.current.setSearchQuery({ query, offset: 0 });
  });

  expect(GiphyApi.search).toHaveBeenCalledTimes(1);
  expect(GiphyApi.search).toHaveBeenCalledWith(query, 0);
  expect(result.current.state.data).toStrictEqual(response);
});
