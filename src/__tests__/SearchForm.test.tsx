import React from "react";
import { render, fireEvent } from "@testing-library/react";
import SearchForm from "../components/SearchForm";

let props = {
  onSubmit: jest.fn()
};

beforeEach(() => {
  props = {
    onSubmit: jest.fn()
  };
});

test("it renders without crashing", () => {
  const { container } = render(<SearchForm {...props} />);

  expect(container).toMatchSnapshot();
});

test("it should propagate search for terms", () => {
  const query = "Funny dogs";

  const { getByTitle } = render(<SearchForm {...props} />);
  const input = getByTitle("query");
  const submit = getByTitle("submit");

  fireEvent.change(input, { target: { value: query } });
  fireEvent.click(submit);

  expect(props.onSubmit).toHaveBeenCalledTimes(1);
  expect(props.onSubmit).toHaveBeenCalledWith(query);
});
