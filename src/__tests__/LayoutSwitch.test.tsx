import React from "react";
import { render, fireEvent } from "@testing-library/react";
import LayoutSwitch, { LAYOUT } from "../components/LayoutSwitch";

const props = { layout: LAYOUT.SINGLE_COLUMN, setLayout: jest.fn() };

beforeEach(() => {
  props.setLayout = jest.fn();
});

test("it renders without crashing", () => {
  const { container } = render(<LayoutSwitch {...props} />);

  expect(container).toMatchSnapshot();
});

test.each`
  layout                  | expected
  ${LAYOUT.SINGLE_COLUMN} | ${true}
  ${LAYOUT.THREE_COLUMN}  | ${false}
`(
  "radio button checked state for layout $layout should be $expected",
  ({ layout, expected }) => {
    const { getByTitle } = render(<LayoutSwitch {...props} />);
    const input = getByTitle(layout);

    if (input instanceof HTMLInputElement) {
      expect(input.checked).toEqual(expected);
    }
  }
);

test("it should change layout when checked", async () => {
  const { getByTitle } = render(<LayoutSwitch {...props} />);
  const input = getByTitle(LAYOUT.THREE_COLUMN);

  fireEvent.click(input);

  expect(props.setLayout).toHaveBeenCalledTimes(1);
  expect(props.setLayout).toHaveBeenCalledWith(LAYOUT.THREE_COLUMN);
});
