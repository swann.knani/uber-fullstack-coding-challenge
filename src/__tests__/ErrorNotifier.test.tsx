import React from "react";
import { render } from "@testing-library/react";
import ErrorNotifier, { DEFAULT_MESSAGE } from "../components/ErrorNotifier";

test("it renders without crashing", () => {
  const { container } = render(<ErrorNotifier />);

  expect(container).toMatchSnapshot();
});

test("it render with default message if none is provided", () => {
  const { container } = render(<ErrorNotifier />);

  expect(container).toHaveTextContent(DEFAULT_MESSAGE);
});

test("it render with correct message if one is provided", () => {
  const message = "Giphy API is awesome";

  const { container } = render(<ErrorNotifier message={message} />);

  expect(container).toHaveTextContent(message);
});
