import { fireEvent } from "@testing-library/react";
import { renderHook, act } from "@testing-library/react-hooks";
import useInfiniteScroll from "../effects/use-infinit-scroll";

test("it should initialize state properly", () => {
  const { result } = renderHook(() => useInfiniteScroll(false));

  expect(result.current.canRequestAdditionalItems).toBe(false);
});

test("it should allow to update its internal state", () => {
  const { result } = renderHook(() => useInfiniteScroll(false));
  act(() => {
    result.current.setCanRequestAdditionalItems(true);
  });
  expect(result.current.canRequestAdditionalItems).toBe(true);
});

// I'm not very proud of these tests because they're quite fragile.
// Indeed, we need to mocked the implementation of windows and document to make them pass
test("should not allow additional items to be fetch when user didn't reach the end of the page", () => {
  const { result } = renderHook(() => useInfiniteScroll(false));
  act(() => {
    fireEvent.scroll(window);
  });

  expect(result.current.canRequestAdditionalItems).toBe(false);
});

test("should allow additional items to be fetch when user did reach the end of the page", () => {
  const baseHeight = 768;

  jest
    .spyOn(document.documentElement, "offsetHeight", "get")
    .mockImplementation(() => baseHeight * 2);

  jest
    .spyOn(document.documentElement, "scrollTop", "get")
    .mockImplementation(() => baseHeight);

  const { result } = renderHook(() => useInfiniteScroll(false));

  act(() => {
    fireEvent.scroll(window);
  });

  expect(result.current.canRequestAdditionalItems).toBe(true);

  jest.resetAllMocks();
});
