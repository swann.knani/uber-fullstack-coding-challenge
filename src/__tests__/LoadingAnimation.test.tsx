import React from "react";
import { render } from "@testing-library/react";
import LoadingAnimation from "../components/LoadingAnimation";

test("it renders without crashing", () => {
  const { container } = render(<LoadingAnimation />);

  expect(container).toMatchSnapshot();
});
