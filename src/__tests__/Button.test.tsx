import React from "react";
import { render } from "@testing-library/react";
import Button from "../components/Button";

test("it renders without crashing", () => {
  const { container } = render(<Button />);

  expect(container).toMatchSnapshot();
});
