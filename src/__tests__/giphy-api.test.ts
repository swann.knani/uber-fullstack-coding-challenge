import axios from "axios";
import createGiphyApi from "../giphy-api";
import {
  GIFApiAuthenticationError,
  GIFApiRequestError,
  GIFApi
} from "../gif-api";
import { GiphyObjects as GiphyObjectsFixture } from "../fixtures";

jest.mock("axios");

const mockedAxios = axios as jest.Mocked<typeof axios>;

const API_PATH = "https://api.giphy.com/v1/gifs";
const API_KEY = "CdRKiCMbTnt9CkZTZ0lGukSczk6iT4Z6";
const query = "Dog";
const offset = 10;

let GiphyApi: GIFApi;

beforeEach(() => {
  jest.resetAllMocks();

  GiphyApi = createGiphyApi(API_PATH, API_KEY);
  mockedAxios.get.mockResolvedValue({ data: GiphyObjectsFixture });
});

describe("search", () => {
  test("it should throw GIFApiAuthenticationError if API_PATH env variable is missing", () => {
    expect(() => {
      createGiphyApi(API_PATH, undefined);
    }).toThrowError(GIFApiAuthenticationError);
  });

  test("it should throw GIFApiAuthenticationError if API_KEY env variable is missing", () => {
    expect(() => {
      createGiphyApi(undefined, API_KEY);
    }).toThrowError(GIFApiAuthenticationError);
  });

  test("should send a GET request with proper params", async () => {
    await GiphyApi.search(query, offset);

    expect(mockedAxios.get).toHaveBeenCalledWith(API_PATH + "/search", {
      params: {
        api_key: API_KEY,
        q: query,
        offset
      }
    });
  });

  test("should return GIFObject properly formated", async () => {
    const response = await GiphyApi.search(query, offset);

    expect(response).toMatchObject([
      {
        height: "76",
        href: "https://gph.is/2n2XYkw",
        id: "Y4pAQv58ETJgRwoLxj",
        src:
          "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-preview.gif",
        title: "adorable dog GIF",
        width: "61"
      },
      {
        height: "100",
        href: "https://gph.is/2lGDRce",
        id: "VkIet63SWUJa0",
        src: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-preview.gif",
        title: "stoned dog GIF",
        width: "111"
      }
    ]);
  });

  test("it should throw GIFApiRequestError if request fails", async () => {
    mockedAxios.get.mockRejectedValue(new Error());

    await expect(GiphyApi.search(query, offset)).rejects.toThrowError(
      GIFApiRequestError
    );
  });
});
