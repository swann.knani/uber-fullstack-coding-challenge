import React from "react";
import { render } from "@testing-library/react";
import GIFCard from "../components/GIFCard";
import { GIFObject as GIFObjectFixture } from "../fixtures";

const { id, ...props } = GIFObjectFixture;

test("it renders without crashing", () => {
  const { container } = render(<GIFCard {...props} />);

  expect(container).toMatchSnapshot();
});

test("it render with correct title", () => {
  const { getByRole } = render(<GIFCard {...props} />);
  const title = getByRole("title");

  expect(title).toHaveTextContent("cat deal with it");
});
