import axios from "axios";
import {
  GIFApi,
  GIFApiAuthenticationError,
  GIFApiRequestError,
  GIFObject
} from "./gif-api";

export default function createGiphyApi(
  ApiPath?: string,
  ApiKey?: string
): GIFApi {
  if (!(ApiPath && ApiKey)) {
    throw new GIFApiAuthenticationError("Missing Giphy Api configuration");
  }

  return {
    search: async (query, offset) => {
      try {
        const {
          data: { data }
        } = await axios.get<SearchReponse>(
          process.env.REACT_APP_GIPHY_API_ENDPOINT + "/search",
          {
            params: {
              api_key: process.env.REACT_APP_GIPHY_API_KEY,
              q: query,
              offset
            }
          }
        );
        return data.map(formatGiphyObjectToGIFObject);
      } catch (err) {
        throw new GIFApiRequestError(
          `An error occurs while fetching ${query} at offset ${offset}`
        );
      }
    }
  };
}

function formatGiphyObjectToGIFObject(giphyObject: GiphyObject): GIFObject {
  return {
    id: giphyObject.id,
    title: giphyObject.title,
    href: giphyObject.bitly_url,
    src: giphyObject.images.preview_gif.url,
    width: giphyObject.images.preview_gif.width,
    height: giphyObject.images.preview_gif.height
  };
}

export type SearchReponse = {
  data: GiphyObject[];
};

export interface GiphyObject {
  id: string;
  bitly_url: string;
  title: string;
  images: GiphyImage;
}

export interface GiphyImage {
  preview_gif: {
    url: string;
    width: string;
    height: string;
  };
}
