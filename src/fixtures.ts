export const GIFObject = {
  id: "hfjkekfk4kcjk",
  title: "cat deal with it GIF",
  href: "https://gph.is/1nV1OL1",
  src: "https://media1.giphy.com/media/lFfLINS1MkZs4/giphy-preview.gif",
  width: "105",
  height: "59"
};

export const GiphyObjects = {
  data: [
    {
      type: "gif",
      id: "Y4pAQv58ETJgRwoLxj",
      slug: "dog-eyebleach-im-flying-Y4pAQv58ETJgRwoLxj",
      url: "https://giphy.com/gifs/dog-eyebleach-im-flying-Y4pAQv58ETJgRwoLxj",
      bitly_gif_url: "https://gph.is/2n2XYkw",
      bitly_url: "https://gph.is/2n2XYkw",
      embed_url: "https://giphy.com/embed/Y4pAQv58ETJgRwoLxj",
      username: "",
      source: "https://www.reddit.com/r/aww/comments/93s0yr/im_flyiiiiing/",
      rating: "pg",
      content_url: "",
      source_tld: "www.reddit.com",
      source_post_url:
        "https://www.reddit.com/r/aww/comments/93s0yr/im_flyiiiiing/",
      is_sticker: 0,
      import_datetime: "2018-08-02 00:25:17",
      trending_datetime: "2019-01-10 11:45:01",
      images: {
        fixed_height_still: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200_s.gif",
          width: "160",
          height: "200",
          size: "16570"
        },
        original_still: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy_s.gif",
          width: "384",
          height: "480",
          size: "97227"
        },
        fixed_width: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200w.gif",
          width: "200",
          height: "250",
          size: "4947142",
          mp4: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200w.mp4",
          mp4_size: "1198944",
          webp: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200w.webp",
          webp_size: "1786322"
        },
        fixed_height_small_still: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100_s.gif",
          width: "80",
          height: "100",
          size: "5925"
        },
        fixed_height_downsampled: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200_d.gif",
          width: "160",
          height: "200",
          size: "107004",
          webp: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200_d.webp",
          webp_size: "55330"
        },
        preview: {
          width: "206",
          height: "258",
          mp4:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-preview.mp4",
          mp4_size: "39987"
        },
        fixed_height_small: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100.gif",
          width: "80",
          height: "100",
          size: "1140024",
          mp4: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100.mp4",
          mp4_size: "351674",
          webp: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100.webp",
          webp_size: "561246"
        },
        downsized_still: {
          url:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-downsized_s.gif",
          width: "257",
          height: "322",
          size: "32425"
        },
        downsized: {
          url:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-downsized.gif",
          width: "257",
          height: "322",
          size: "1593861"
        },
        downsized_large: {
          url:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-downsized-large.gif",
          width: "384",
          height: "480",
          size: "5953692"
        },
        fixed_width_small_still: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100w_s.gif",
          width: "100",
          height: "125",
          size: "9369"
        },
        preview_webp: {
          url:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-preview.webp",
          width: "116",
          height: "144",
          size: "42206"
        },
        fixed_width_still: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200w_s.gif",
          width: "200",
          height: "250",
          size: "24142"
        },
        fixed_width_small: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100w.gif",
          width: "100",
          height: "125",
          size: "1684971",
          mp4: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100w.mp4",
          mp4_size: "43394",
          webp: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/100w.webp",
          webp_size: "758722"
        },
        downsized_small: {
          width: "120",
          height: "150",
          mp4:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-downsized-small.mp4",
          mp4_size: "178336"
        },
        fixed_width_downsampled: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200w_d.gif",
          width: "200",
          height: "250",
          size: "146692",
          webp: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200w_d.webp",
          webp_size: "80364"
        },
        downsized_medium: {
          url:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-downsized-medium.gif",
          width: "384",
          height: "480",
          size: "4485567"
        },
        original: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy.gif",
          width: "384",
          height: "480",
          size: "20941000",
          frames: "225",
          mp4: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy.mp4",
          mp4_size: "2886228",
          webp: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy.webp",
          webp_size: "3747768",
          hash: "92c25d235164a8f65d0e80ad73896020"
        },
        fixed_height: {
          url: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200.gif",
          width: "160",
          height: "200",
          size: "3487948",
          mp4: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200.mp4",
          mp4_size: "868273",
          webp: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/200.webp",
          webp_size: "1348338"
        },
        hd: {
          width: "720",
          height: "900",
          mp4: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-hd.mp4",
          mp4_size: "5868451"
        },
        looping: {
          mp4:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-loop.mp4",
          mp4_size: "2626666"
        },
        original_mp4: {
          width: "384",
          height: "480",
          mp4: "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy.mp4",
          mp4_size: "2886228"
        },
        preview_gif: {
          url:
            "https://media3.giphy.com/media/Y4pAQv58ETJgRwoLxj/giphy-preview.gif",
          width: "61",
          height: "76",
          size: "49655"
        },
        "480w_still": {
          url: "https://media4.giphy.com/media/Y4pAQv58ETJgRwoLxj/480w_s.jpg",
          width: "480",
          height: "600"
        }
      },
      title: "adorable dog GIF",
      analytics: {
        onload: {
          url:
            "https://giphy-analytics.giphy.com/simple_analytics?response_id=0d136a9e104f81c7f7468cf07e8b8b7d1be8e101&event_type=GIF_SEARCH&gif_id=Y4pAQv58ETJgRwoLxj&action_type=SEEN"
        },
        onclick: {
          url:
            "https://giphy-analytics.giphy.com/simple_analytics?response_id=0d136a9e104f81c7f7468cf07e8b8b7d1be8e101&event_type=GIF_SEARCH&gif_id=Y4pAQv58ETJgRwoLxj&action_type=CLICK"
        },
        onsent: {
          url:
            "https://giphy-analytics.giphy.com/simple_analytics?response_id=0d136a9e104f81c7f7468cf07e8b8b7d1be8e101&event_type=GIF_SEARCH&gif_id=Y4pAQv58ETJgRwoLxj&action_type=SENT"
        }
      }
    },
    {
      type: "gif",
      id: "VkIet63SWUJa0",
      slug: "dude-VkIet63SWUJa0",
      url: "https://giphy.com/gifs/dude-VkIet63SWUJa0",
      bitly_gif_url: "https://gph.is/2lGDRce",
      bitly_url: "https://gph.is/2lGDRce",
      embed_url: "https://giphy.com/embed/VkIet63SWUJa0",
      username: "",
      source:
        "https://www.reddit.com/r/gifs/comments/5w4msb/dude_am_i_swimming/",
      rating: "g",
      content_url: "",
      source_tld: "www.reddit.com",
      source_post_url:
        "https://www.reddit.com/r/gifs/comments/5w4msb/dude_am_i_swimming/",
      is_sticker: 0,
      import_datetime: "2017-02-25 15:48:09",
      trending_datetime: "2019-06-30 04:45:01",
      images: {
        fixed_height_still: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/200_s.gif",
          width: "222",
          height: "200",
          size: "15121"
        },
        original_still: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy_s.gif",
          width: "347",
          height: "312",
          size: "39151"
        },
        fixed_width: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/200w.gif",
          width: "200",
          height: "180",
          size: "594906",
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/200w.mp4",
          mp4_size: "117267",
          webp: "https://media3.giphy.com/media/VkIet63SWUJa0/200w.webp",
          webp_size: "423334"
        },
        fixed_height_small_still: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/100_s.gif",
          width: "111",
          height: "100",
          size: "4736"
        },
        fixed_height_downsampled: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/200_d.gif",
          width: "222",
          height: "200",
          size: "94416",
          webp: "https://media3.giphy.com/media/VkIet63SWUJa0/200_d.webp",
          webp_size: "65364"
        },
        preview: {
          width: "150",
          height: "134",
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-preview.mp4",
          mp4_size: "40260"
        },
        fixed_height_small: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/100.gif",
          width: "111",
          height: "100",
          size: "218835",
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/100.mp4",
          mp4_size: "46523",
          webp: "https://media3.giphy.com/media/VkIet63SWUJa0/100.webp",
          webp_size: "174474"
        },
        downsized_still: {
          url:
            "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-downsized_s.gif",
          width: "347",
          height: "312",
          size: "39151"
        },
        downsized: {
          url:
            "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-downsized.gif",
          width: "347",
          height: "312",
          size: "1857944"
        },
        downsized_large: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy.gif",
          width: "347",
          height: "312",
          size: "1857944"
        },
        fixed_width_small_still: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/100w_s.gif",
          width: "100",
          height: "90",
          size: "3997"
        },
        preview_webp: {
          url:
            "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-preview.webp",
          width: "133",
          height: "120",
          size: "49544"
        },
        fixed_width_still: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/200w_s.gif",
          width: "200",
          height: "180",
          size: "12358"
        },
        fixed_width_small: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/100w.gif",
          width: "100",
          height: "90",
          size: "183757",
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/100w.mp4",
          mp4_size: "41169",
          webp: "https://media3.giphy.com/media/VkIet63SWUJa0/100w.webp",
          webp_size: "150590"
        },
        downsized_small: {
          width: "241",
          height: "218",
          mp4:
            "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-downsized-small.mp4",
          mp4_size: "164058"
        },
        fixed_width_downsampled: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/200w_d.gif",
          width: "200",
          height: "180",
          size: "77406",
          webp: "https://media3.giphy.com/media/VkIet63SWUJa0/200w_d.webp",
          webp_size: "54988"
        },
        downsized_medium: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy.gif",
          width: "347",
          height: "312",
          size: "1857944"
        },
        original: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy.gif",
          width: "347",
          height: "312",
          size: "1857944",
          frames: "46",
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy.mp4",
          mp4_size: "558458",
          webp: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy.webp",
          webp_size: "1265790",
          hash: "2b593ebc1de17f7cbe879b3cbf8baa87"
        },
        fixed_height: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/200.gif",
          width: "222",
          height: "200",
          size: "726083",
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/200.mp4",
          mp4_size: "132352",
          webp: "https://media3.giphy.com/media/VkIet63SWUJa0/200.webp",
          webp_size: "506164"
        },
        looping: {
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-loop.mp4",
          mp4_size: "2249023"
        },
        original_mp4: {
          width: "480",
          height: "430",
          mp4: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy.mp4",
          mp4_size: "558458"
        },
        preview_gif: {
          url: "https://media3.giphy.com/media/VkIet63SWUJa0/giphy-preview.gif",
          width: "111",
          height: "100",
          size: "48173"
        },
        "480w_still": {
          url: "https://media2.giphy.com/media/VkIet63SWUJa0/480w_s.jpg",
          width: "480",
          height: "432"
        }
      },
      title: "stoned dog GIF",
      analytics: {
        onload: {
          url:
            "https://giphy-analytics.giphy.com/simple_analytics?response_id=0d136a9e104f81c7f7468cf07e8b8b7d1be8e101&event_type=GIF_SEARCH&gif_id=VkIet63SWUJa0&action_type=SEEN"
        },
        onclick: {
          url:
            "https://giphy-analytics.giphy.com/simple_analytics?response_id=0d136a9e104f81c7f7468cf07e8b8b7d1be8e101&event_type=GIF_SEARCH&gif_id=VkIet63SWUJa0&action_type=CLICK"
        },
        onsent: {
          url:
            "https://giphy-analytics.giphy.com/simple_analytics?response_id=0d136a9e104f81c7f7468cf07e8b8b7d1be8e101&event_type=GIF_SEARCH&gif_id=VkIet63SWUJa0&action_type=SENT"
        }
      }
    }
  ],
  pagination: {
    total_count: 85264,
    count: 25,
    offset: 25
  },
  meta: {
    status: 200,
    msg: "OK",
    response_id: "0d136a9e104f81c7f7468cf07e8b8b7d1be8e101"
  }
};
