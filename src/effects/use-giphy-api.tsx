import { useState, useEffect, useReducer, useRef } from "react";
import { GIFObject, GIFApi } from "../gif-api";

export default (
  Api: GIFApi,
  initialQuery: string,
  initialOffset: number
): {
  state: State;
  setSearchQuery: React.Dispatch<React.SetStateAction<SearchQuery>>;
} => {
  const ApiRef = useRef<GIFApi>();

  useEffect(() => {
    ApiRef.current = Api;
  }, [Api]);

  const [searchQuery, setSearchQuery] = useState({
    query: initialQuery,
    offset: initialOffset
  });

  const [state, dispatch] = useReducer(reducer, {
    isLoading: false,
    isError: false,
    data: []
  });

  useEffect(() => {
    let ignore = false;

    const { query, offset } = searchQuery;

    const search = async () => {
      if (!query) {
        return;
      }

      if (offset === 0) {
        dispatch({ type: "FETCH_RESET" });
      }

      dispatch({ type: "FETCH_INIT" });

      try {
        const GIFObjects = await ApiRef.current!.search(query, offset);
        if (!ignore) {
          dispatch({ type: "FETCH_SUCCESS", payload: GIFObjects });
        }
      } catch (error) {
        if (!ignore) {
          dispatch({ type: "FETCH_FAILURE" });
        }
      }
    };

    search();

    return () => {
      ignore = true;
    };
  }, [searchQuery]);

  return { state, setSearchQuery };
};

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case "FETCH_RESET":
      return {
        isLoading: false,
        isError: false,
        data: []
      };
    case "FETCH_INIT":
      return {
        ...state,
        isLoading: true,
        isError: false
      };

    case "FETCH_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        // Giphy API returns sometimes duplicates. See README for more complete explanations
        data: getUniqGIFObjects([...state.data, ...action.payload])
      };
    case "FETCH_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true
      };
    default:
      throw new Error();
  }
};

function getUniqGIFObjects(GIFObjects: GIFObject[]): GIFObject[] {
  return [
    ...GIFObjects.reduce((acc, object) => {
      acc.has(object.id) || acc.set(object.id, object);
      return acc;
    }, new Map<string, GIFObject>()).values()
  ];
}

type Action =
  | { type: "FETCH_RESET" }
  | { type: "FETCH_INIT" }
  | { type: "FETCH_SUCCESS"; payload: GIFObject[] }
  | { type: "FETCH_FAILURE" };

interface State {
  isLoading: boolean;
  isError: boolean;
  data: GIFObject[];
}

interface SearchQuery {
  query: string;
  offset: number;
}
