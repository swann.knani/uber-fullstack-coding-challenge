import { useState, useEffect, useCallback } from "react";

export default (
  initialValue: boolean
): {
  canRequestAdditionalItems: boolean;
  setCanRequestAdditionalItems: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const [canRequestAdditionalItems, setCanRequestAdditionalItems] = useState<
    boolean
  >(initialValue);

  const handleScroll = useCallback(() => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
        document.documentElement.offsetHeight ||
      canRequestAdditionalItems
    )
      return;
    setCanRequestAdditionalItems(true);
  }, [canRequestAdditionalItems]);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [handleScroll]);

  return { canRequestAdditionalItems, setCanRequestAdditionalItems };
};
