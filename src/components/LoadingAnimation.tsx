import React from "react";
import styled from "styled-components";
import { ReactComponent as AnimatedBarIcon } from "../svg/animated-bar-icon.svg";

const LoadingAnimation: React.FC = () => (
  <Container>
    <AnimatedBarIcon />
  </Container>
);

export default LoadingAnimation;

const Container = styled.div`
  display: flex;
  height: var(--footer-height);
  justify-content: center;
  align-items: center;
  background-color: var(--white);

  svg path,
  svg rect {
    fill: var(--black);
  }
`;
