import React, { useEffect, useCallback, useRef } from "react";
import styled from "styled-components";
import useGiphyApi from "../effects/use-giphy-api";
import useInfiniteScroll from "../effects/use-infinit-scroll";
import { GIFObject } from "../gif-api";
import createGiphyApi from "../giphy-api";

const GiphyApi = createGiphyApi(
  process.env.REACT_APP_GIPHY_API_ENDPOINT,
  process.env.REACT_APP_GIPHY_API_KEY
);

const INITAL_OFFSET = 0;
const OFFSET_THRESHOLD = 25;

const GiphyExplorer: React.FC<GiphyExplorerProps> = ({ query, render }) => {
  const offset = useRef(INITAL_OFFSET);

  const {
    state: { isLoading, isError, data },
    setSearchQuery
  } = useGiphyApi(GiphyApi, query, offset.current);

  const {
    canRequestAdditionalItems,
    setCanRequestAdditionalItems
  } = useInfiniteScroll(false);

  const shouldRequestAdditionalItems = useCallback(
    () => !isLoading && !isError && canRequestAdditionalItems,
    [isLoading, isError, canRequestAdditionalItems]
  );

  useEffect(() => {
    offset.current = INITAL_OFFSET;
    setSearchQuery(makeSeachQuery(query, offset.current));
  }, [query, setSearchQuery]);

  useEffect(() => {
    if (shouldRequestAdditionalItems()) {
      setCanRequestAdditionalItems(false);
      offset.current += OFFSET_THRESHOLD;
      setSearchQuery(makeSeachQuery(query, offset.current));
    }
  }, [
    setCanRequestAdditionalItems,
    shouldRequestAdditionalItems,
    query,
    setSearchQuery
  ]);

  return <Wrapper>{render(data, isLoading, isError)}</Wrapper>;
};

export default GiphyExplorer;

function makeSeachQuery(query: string, offset: number) {
  return {
    query,
    offset
  };
}

interface GiphyExplorerProps {
  query: string;
  render: (
    data: GIFObject[],
    isLoading: boolean,
    isError: boolean
  ) => React.ReactNode;
}

const Wrapper = styled.div`
  padding-bottom: var(--footer-height);
`;
