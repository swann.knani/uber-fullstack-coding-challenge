import styled from "styled-components";

const Button = styled.button`
  display: inline-flex;
  flex: 0 1 auto;
  width: var(--button-size);
  height: var(--button-size);
  border: 0;
  padding: 0;
  justify-content: center;
  background: none;
  border: var(--border);
  transition: background-color 150ms ease-in-out;
  cursor: pointer;
  background-color: var(--white);

  svg {
    width: calc(var(--button-size) / 2);
    fill: var(--black);
    transition: fill 150ms ease-in-out;

    fill: var(--black);
  }

  &:hover {
    background-color: var(--black);

    svg {
      fill: var(--white);
    }
  }
`;

export default Button;
