import React from "react";
import styled from "styled-components";

export const GIFCard: React.FC<GIFCardProps> = ({
  title,
  href,
  src,
  width,
  height
}) => {
  return (
    <Figure>
      <Link href={href} title={title} target="_blank" rel="noopener noreferrer">
        <Image
          src={src}
          width={width}
          height={height}
          title={title}
          alt={title}
        />
        <Caption role="title">{formatTitle(title)}</Caption>
      </Link>
    </Figure>
  );
};

export default GIFCard;

function formatTitle(title: string): string {
  return title.replace(/\sGIF$/gi, "");
}

const Caption = styled.figcaption`
  position: absolute;
  text-align: center;
  width: 100%;
  top: 20%;
  left: 50%;
  padding: 0 2rem;
  opacity: 0;
  color: white;
  transform: translateX(-50%);
  text-transform: uppercase;
  transition: all 150ms ease-in-out 0s;
  z-index: 2;
`;

const Figure = styled.figure`
  position: relative;
  margin: 0;
  padding: 0;
  width: var(--gallery-item-size);
  height: var(--gallery-item-size);
  background: var(--black);
  &:after {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    opacity: 0;
    background: rgba(0, 0, 0, 0.7);
    transition: opacity 200ms ease-in-out 0s;
  }

  :hover::after {
    opacity: 1;
  }

  :hover ${Caption} {
    top: 50%;
    opacity: 1;
  }
`;

const Link = styled.a`
  display: block;
  position: relative;
  width: 100%;
  height: 100%;
`;

const Image = styled.img`
  width: 100% !important;
  height: 100% !important;
  object-fit: cover;
`;

interface GIFCardProps {
  title: string;
  href: string;
  src: string;
  width: string;
  height: string;
}
