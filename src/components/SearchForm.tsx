import React, { useRef } from "react";
import styled from "styled-components";
import Button from "./Button";
import { ReactComponent as SearchIcon } from "../svg/search-icon.svg";

const SearchForm: React.FC<SearchFormProps> = ({ onSubmit }) => {
  const ref = useRef<HTMLInputElement>(null);

  const onQueryChange = (event: React.FormEvent) => {
    event.preventDefault();
    if (ref && ref.current) {
      onSubmit(ref.current.value);
    }
  };

  return (
    <Form onSubmit={onQueryChange}>
      <Button type="submit" title="submit">
        <SearchIcon />
      </Button>

      <Input
        ref={ref}
        type="text"
        name="query"
        title="query"
        id="query"
        placeholder="Type your search query"
        required
      />
    </Form>
  );
};

export default SearchForm;

const Form = styled.form`
  display: flex;
  align-items: center;
  flex: 1 0 auto;
  margin-right: 20px;
`;

const Input = styled.input`
  height: var(--button-size);
  padding: 10px;
  flex: 1 1 auto;
  border: var(--border);
  margin-left: calc(var(--button-size) / 2);
`;

interface SearchFormProps {
  onSubmit: React.Dispatch<React.SetStateAction<string>>;
}
