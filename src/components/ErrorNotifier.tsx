import React from "react";
import styled from "styled-components";

const ErrorNotifier: React.FC<ErrorNotifierProps> = ({ message }) => (
  <Container>
    <Content>{message || DEFAULT_MESSAGE}</Content>
  </Container>
);

export default ErrorNotifier;

const Container = styled.div`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 400px;
  bottom: 0px;
  left: 50%;
  padding: 10px 20px;
  transform: translate(-50%, 0);
  color: var(--white);
  background: var(--black);
  z-index: 1;
`;

const Content = styled.span``;

export const DEFAULT_MESSAGE = "Something went wrong...";

interface ErrorNotifierProps {
  message?: string;
}
