import React from "react";
import styled from "styled-components";
import Button from "./Button";

import { ReactComponent as SingleColumnLayoutIcon } from "../svg/single-column-layout-icon.svg";
import { ReactComponent as ThreeColumnLayoutIcon } from "../svg/three-column-layout-icon.svg";

const LayoutSwitch: React.FC<LayoutSwitchProps> = ({ layout, setLayout }) => {
  const handleClick = (layout: LAYOUT) => () => setLayout(layout);

  return (
    <Container>
      {LAYOUTS.map(([type, Icon]) => (
        <Label key={type} htmlFor={type}>
          <Input
            type="checkbox"
            id={type}
            title={type}
            checked={type === layout}
            onChange={handleClick(type)}
          />
          <Button as="span">
            <Icon />
          </Button>
        </Label>
      ))}
    </Container>
  );
};

export default LayoutSwitch;

export enum LAYOUT {
  "SINGLE_COLUMN" = "SINGLE_COLUMN",
  "THREE_COLUMN" = "THREE_COLUMN"
}

const LAYOUTS: [LAYOUT, React.FunctionComponent][] = [
  [LAYOUT.SINGLE_COLUMN, SingleColumnLayoutIcon],
  [LAYOUT.THREE_COLUMN, ThreeColumnLayoutIcon]
];

const Container = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
`;

const Input = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`;

const Label = styled.label`
  display: block;
  position: relative;
  cursor: pointer;
  user-select: none;

  &:not(:last-child) {
    margin-right: calc(var(--button-size) / 2);
  }

  ${Input}:checked ~ ${Button} {
    background-color: var(--black);
  }

  ${Input}:checked ~ ${Button} svg {
    fill: var(--white);
  }
`;

interface LayoutSwitchProps {
  layout: LAYOUT;
  setLayout: React.Dispatch<React.SetStateAction<LAYOUT>>;
}
