import React, { useState } from "react";
import styled, { createGlobalStyle, css } from "styled-components";
import SearchForm from "./components/SearchForm";
import LayoutSwitch, { LAYOUT } from "./components/LayoutSwitch";
import GiphyExplorer from "./components/GiphyExplorer";
import LoadingAnimation from "./components/LoadingAnimation";
import GIFCard from "./components/GIFCard";
import ErrorNotifier from "./components/ErrorNotifier";
import { ReactComponent as GiphyIcon } from "./svg/giphy-icon.svg";
import { ReactComponent as UberIcon } from "./svg/uber-icon.svg";

const App: React.FC = () => {
  const [query, setQuery] = useState<string>("");
  const [layout, setLayout] = useState<LAYOUT>(LAYOUT.SINGLE_COLUMN);

  return (
    <>
      <GlobalStyle />
      <Header>
        <SearchForm onSubmit={setQuery} />
        <LayoutSwitch layout={layout} setLayout={setLayout} />
      </Header>
      <Main>
        <GiphyExplorer
          query={query}
          render={(GIFObjects, isLoading, isError) => (
            <>
              <Container layout={layout}>
                {GIFObjects.map(({ id, ...rest }) => (
                  <GIFCard key={id} {...rest} />
                ))}
              </Container>
              {isLoading && <LoadingAnimation />}
              {isError && <ErrorNotifier />}
            </>
          )}
        ></GiphyExplorer>
      </Main>
      <Footer>
        <Icon as={UberIcon} />
        <span role="img" aria-label="heart suit">
          ♥️
        </span>
        <Icon as={GiphyIcon} />
      </Footer>
    </>
  );
};

export default App;

const GlobalStyle = createGlobalStyle`
  @import url("https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap");
  @import-normalize;

  :root {
    --white: #ffffff;
    --black: #000000;
    --blue: #276ef1;
    --grid-column-gap: 20px;
    --grid-row-gap: 20px;
    --header-height: 80px;
    --footer-height: 40px;
    --button-size: 40px;
    --gallery-item-size: 250px;
    --border: 1px solid var(--black);
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  html {
    box-sizing: border-box;
    font-family: "Roboto", sans-serif;
  }

  body {
    margin: 0;
  }
`;

const banner = css`
  display: flex;
  position: fixed;
  width: 100%;
  left: 0;
  padding: 0 40px;
  align-items: center;
  background: var(--white);
  z-index: 99;
`;

const Header = styled.header`
  ${banner}
  height: var(--header-height);
  top: 0;
  flex: 1 0 auto;
  justify-content: space-between;
  border-bottom: 2px solid var(--black);
`;

const Footer = styled.footer`
  ${banner}
  color: red;
  font-size: 0.8rem;
  bottom: 0
  height: var(--footer-height);
`;

const Main = styled.main`
  min-height: 100vh;
  padding: var(--header-height) 0 var(--footer-height);
`;

const Container = styled.div<ContainerProps>`
  display: grid;
  grid-template-columns: ${props =>
    `repeat(${props.layout === LAYOUT.SINGLE_COLUMN ? 1 : 3}, min-content);`};
  justify-content: center;
  grid-column-gap: var(--grid-column-gap);
  grid-row-gap: var(--grid-row-gap);
  padding: calc(var(--grid-row-gap) * 2.5) var(--grid-column-gap);
`;

const Icon = styled.div`
  height: calc(var(--button-size) / 3);
  margin: 0 0.4rem;
`;

interface ContainerProps {
  readonly layout: LAYOUT;
}
