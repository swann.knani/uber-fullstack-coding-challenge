export interface GIFObject {
  id: string;
  title: string;
  href: string;
  src: string;
  width: string;
  height: string;
}

export interface GIFApi {
  search: (query: string, offset: number) => Promise<GIFObject[]>;
}

export class GIFApiAuthenticationError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}

export class GIFApiRequestError extends Error {
  constructor(message?: string) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
  }
}
