## 🌍 Application

The application is deployed on Heroku at the following address:

https://uber-giphy-swann-knani.herokuapp.com/

## 🐴 Installation

- Copy `.env.example` to `.env` and fill its content with valid `REACT_APP_GIPHY_API_ENDPOINT` and `REACT_APP_GIPHY_API_KEY` variables

## 📦 Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.

## 🛠 Tools

### Typescript

I implemented the project with Typescript. It allows to have strong typing in javascript.<br>
It allows to better represent the different states of the application, the possible values that a function accepts as an argument, etc<br>
It also avoids writing unit tests to test the different edges cases concerning prohibited types

### Styled components

Styled Components help keep the concerns of styling and element architecture separated and make components more readable.<br>
Furthermore, when you have components that rely on JavaScript for their style, Styled Components gives control of those states back to CSS instead of using a multitude of conditional class names.

## What did I tried to do ?

- Design the API in a way that it can be interchangeable with another source as long as it follows the same interface.

- Use react hooks to extract some part of code that can be reused if we want to implement the same behaviour in another part of the application.

## 💪 What can be improved ?

### Code

- I had to create a `getUniqGIFObjects` function to filter duplicates. Indeed the Giphy API seems to have [several issues](https://github.com/Giphy/GiphyAPI/issues?utf8=%E2%9C%93&q=is%3Aissue+duplicate) regarding calls returning duplicated GIF object.<br>
  I added to the project 2 screenshots [3rd call](./api_call_3.png) and [4th call](./api_call_4.png) showing that subsequent API calls return sometimes GIF with same ids. `total_count` property seems also to be busted.
- Add lazy loading on `GIFCard`
- Add a modal to display the full GIF when clicked and not only the first few frames
- Add more unit tests
- Improve accessibility
- Improve error handling

### Design

- Improve the actual design to be pixel perfect
- Improve display for mobile/tablet
